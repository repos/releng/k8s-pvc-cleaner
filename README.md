## Kubernetes PVC Cleaner
Use case:

When you use a StatefulSet with volumeClaimTemplates, the PVCs that
are automatically created when the StatefulSet scales up are not
automatically deleted when the StatefulSet scales down.  This is
reasonable behavior for data safety.  However, if the data in the PVCs
is not critical, for example, if it is used for a cache, it can be
cost-effective to automatically delete PVCs from scaled-down
StatefulSets.

k8s-pvc-cleaner serves this purpose by periodically monitoring when PVCs
are used by pods and automatically deleting PVCs that have not been
used longer than a specified amount of time.

## Features
* Automated Deletion: Detects and deletes unused PVCs within a specified Kubernetes namespace.
* Customization: Allows filtering of PVCs and Pods using regular expressions.
* Safe Testing: Includes a dry-run mode for testing without actual deletions.
* Real-Time Monitoring: Utilizes Kubernetes watch API for dynamic PVC monitoring.

## Usage
To use the PVC cleaner, follow these steps:

Clone this repository to your local machine:

```
git clone git@gitlab.wikimedia.org:repos/releng/k8s-pvc-cleaner.git 
cd k8s-pvc-cleaner
pip install -r requirements.txt

```
## DESCRIPTION
This script is designed to delete unused Persistent Volume Claims (PVCs) within a specified Kubernetes namespace. It operates based on the following parameters:
```
-n <NAMESPACE>: The namespace to monitor (required).
-t <TIMEOUT>: The PVC idle timeout in minutes (default: 240 minutes).
--pvc-filter <PVC_FILTER>: Regex filtering PVCs within the namespace (default: ^.*$).
--pod-filter <POD_FILTER>: Regex filtering Pods within the namespace (default: ^.*$).
--polling-interval <POLLING_INTERVAL>: Polling interval in seconds (default: 30 seconds).
--dry-run: Enables dry-run mode, simulating deletions without actual removal.
--debug: Enables debug logging for more detailed output.

```

To run the script with the desired options, execute the following command, replacing the placeholders with actual values:
```
python pvc-cleaner.py -n <namespace> [-t <timeout>] [--pvc-filter <pvc>] [--pod-filter <pod>] [--polling-interval <interval>] [--dry-run]
```

## Example 
This will delete all the unused PVCs (which are not used by pods) in the given namespace:

```
$ python pvc-cleaner.py -n test-namespace --dry-run -t 240
2024-01-31 14:30:06.792 [root] [INFO] - namespace: test-namespace
2024-01-31 14:30:06.792 [root] [INFO] - timeout: 240
2024-01-31 14:30:06.792 [root] [INFO] - pod_filter: ^.*$
2024-01-31 14:30:06.792 [root] [INFO] - pvc_filter: ^.*$
2024-01-31 14:30:06.792 [root] [INFO] - polling_interval: 30
2024-01-31 14:30:06.792 [root] [INFO] - dry_run: True
2024-01-31 14:31:08.673 [root] [INFO] - PVC test-pvc is eligible for deletion
2024-01-31 14:31:08.673 [root] [INFO] - Dry run enabled, would attempt to delete PVC: test-pvc
```

## Output Explanation
When you run the pvc-cleaner.py script with the specified parameters, the output will include the following information:

Namespace: This confirms the Kubernetes namespace being targeted by the script. In the example, it's test-namespace.

Timeout: Displays the timeout value set in minutes. For example in command, it shows 240 minutes, indicating that the script is looking for PVCs that haven't been used for specified amount of time.

PVC Filter: A regular expression filter for the PVCs. The default value ^.*$ shown in the example means it matches all PVCs. This is useful for narrowing down the target PVCs based on specific patterns in their names.

Pod Filter: Similar to the PVC filter, this is a regular expression used to filter the pods. It helps in identifying which pods to consider while assessing if a PVC is in use. For example, a pod filter of ^.*$ (default) would consider all pods in the namespace. 

Polling Interval: This indicates the time interval, in seconds, between successive checks by the script for unused PVCs. For example, 30 seconds means the script checks for unused PVCs every 30 seconds.

Dry Run: This boolean value shows whether the script is running in dry-run mode. A False value, as in the example, means the script will perform actual deletions of the unused PVCs. If it were True, the script would only simulate the deletion process without actually removing any PVCs, and it will output a message "Dry run enabled, would attempt to delete PVC: test-pvc".

Deleting PVC: For each PVC that the script identifies for deletion, it will output a message like "Deleting PVC: test-pvc". This confirms the specific PVC being deleted in the given namespace.

## Resource 
[Kubernetes Persistent Volumes](https://kubernetes.io/docs/concepts/storage/persistent-volumes/).
This link provides detailed information about Persistent Volumes and PVCs in Kubernetes, explaining their purpose, usage, and how they interact with other Kubernetes resources.

[Kubernetes StatefulSets]( https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/).
This resource offers an in-depth understanding of StatefulSets in Kubernetes, ideal for users who need to manage stateful applications within their Kubernetes environment.