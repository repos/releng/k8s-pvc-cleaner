#!/usr/bin/env python3

import threading
from kubernetes import client, config, watch
import logging
import argparse
import time
import re
import urllib3


# Define the class for PVC record keeping
class PVCRecord:
    def __init__(self, name):
        self.name = name
        self.last_used_time = time.time()
        self.used_by = set()

    def __str__(self):
        return f"<PVC: {self.name}, last_used: {time.ctime(self.last_used_time)}, used_by: {self.used_by}>"

    def touch(self):
        self.last_used_time = time.time()

    def idle_time(self):
        return time.time() - self.last_used_time

    def in_use(self) -> bool:
        return bool(self.used_by)

    def expired(self, threshold) -> bool:
        return self.idle_time() >= threshold

    def add_reference(self, pod_name):
        self.used_by.add(pod_name)
        self.touch()

    def drop_reference(self, pod_name):
        if pod_name in self.used_by:
            self.used_by.remove(pod_name)
        self.touch()


# Define the class for PVC database management
class PVCDatabase:
    def __init__(
        self, api_instance, namespace, threshold, pvc_filter, pod_filter, dry_run
    ):
        self.api_instance = api_instance
        self.namespace = namespace
        self.threshold = threshold
        self.pvc_filter = pvc_filter
        self.pod_filter = pod_filter
        self.dry_run = dry_run

        self.lock = threading.Lock()
        self.records = {}
        self.refresh()

        self.worker_thread_exited = threading.Event()
        self.watch_pods_thread = threading.Thread(
            target=self.watch_pods, name="Pod watcher", daemon=True
        )
        self.watch_pvcs_thread = threading.Thread(
            target=self.watch_pvcs, name="PVC watcher", daemon=True
        )
        self.watch_pods_thread.start()
        self.watch_pvcs_thread.start()

    def __str__(self):
        res = ["PVCDatabase:"]
        with self.lock:
            for pvc_name, record in self.records.items():
                res.append(str(record))
        res.append("--")
        return "\n".join(res)

    def get_filtered_pvcs(self, pvc_filter):
        # Retrieve filtered PVC list
        res = self.api_instance.list_namespaced_persistent_volume_claim(
            namespace=self.namespace
        )
        all_pvcs = res.items

        if pvc_filter:
            filtered_pvcs = [
                pvc for pvc in all_pvcs if re.search(self.pvc_filter, pvc.metadata.name)
            ]
        else:
            filtered_pvcs = all_pvcs
        return filtered_pvcs, res.metadata.resource_version

    def get_filtered_pods(self, pod_filter):
        # Retrieve filtered pod list based on pod_filter
        res = self.api_instance.list_namespaced_pod(namespace=self.namespace)
        all_pods = res.items

        if pod_filter:
            filtered_pods = [
                pod for pod in all_pods if re.search(self.pod_filter, pod.metadata.name)
            ]
        else:
            filtered_pods = all_pods

        return filtered_pods, res.metadata.resource_version

    def refresh(self):
        # Retrieve and refresh PVC and pod Information
        with self.lock:
            pvcs, self.pvcs_resource_version = self.get_filtered_pvcs(self.pvc_filter)
            for pvc in pvcs:
                self.get_record(pvc.metadata.name)

            pods, self.pods_resource_version = self.get_filtered_pods(self.pod_filter)
            for pod in pods:
                for pvc_ref in self.get_pod_filtered_pvc_refs(pod):
                    record = self.get_record(pvc_ref)
                    record.add_reference(pod.metadata.name)
        logging.debug(f"Refreshed PVC db:\n{self}")

    def get_pod_filtered_pvc_refs(self, pod):
        # Return a list of names of PVCs used by `pod` which match the pvc filter
        res = []
        for volume in pod.spec.volumes:
            if volume.persistent_volume_claim:
                pvc_name = volume.persistent_volume_claim.claim_name
                if re.search(self.pvc_filter, pvc_name):
                    res.append(pvc_name)
        return res

    def get_record(self, pvc_name):
        # Retrieve PVC record for the given PVC name
        record = self.records.get(pvc_name)
        if not record:  # If no record exists, create a new one
            record = PVCRecord(pvc_name)
            self.records[pvc_name] = record
        return record

    def delete_record(self, pvc_name):
        if pvc_name in self.records:
            del self.records[pvc_name]

    def get_idlers(self, threshold):
        with self.lock:
            res = []
            for pvc_name, record in self.records.items():
                if not record.in_use() and record.expired(threshold):
                    res.append(pvc_name)

        return res

    def watch_events(self, name, function, resource_version):
        try:
            while True:
                logging.debug(f"watch_events top for {name}")
                try:
                    w = watch.Watch()
                    for event in w.stream(
                        function,
                        namespace=self.namespace,
                        resource_version=resource_version,
                        _request_timeout=60,
                    ):
                        yield event
                        resource_version = event["object"].metadata.resource_version
                except (
                    urllib3.exceptions.ReadTimeoutError,
                    urllib3.exceptions.ProtocolError,
                ):
                    pass
                except Exception as e:
                    logging.error(f"{name} caught exception {e} of type {type(e)}")
                    raise e
                    # logging.error(
                    #     f"watch_pods caught exception {e}\nSleeping for 10 seconds and then restarting."
                    # )
                    # time.sleep(10)
        finally:
            self.worker_thread_exited.set()

    def watch_pods(self):
        for event in self.watch_events(
            "watch_pods",
            self.api_instance.list_namespaced_pod,
            self.pods_resource_version,
        ):
            pod = event["object"]
            if pod.kind != "Pod":
                continue

            logging.debug(f"watch_pods: Pod {pod.metadata.name} {event['type']}")

            with self.lock:
                if event["type"] == "ADDED":
                    for pvc_ref in self.get_pod_filtered_pvc_refs(pod):
                        record = self.get_record(pvc_ref)
                        record.add_reference(pod.metadata.name)
                elif event["type"] == "DELETED":
                    for pvc_ref in self.get_pod_filtered_pvc_refs(pod):
                        record = self.get_record(pvc_ref)
                        record.drop_reference(pod.metadata.name)

    def watch_pvcs(self):
        for event in self.watch_events(
            "watch_pvcs",
            self.api_instance.list_namespaced_persistent_volume_claim,
            self.pvcs_resource_version,
        ):
            pvc = event["object"]
            if pvc.kind != "PersistentVolumeClaim":
                continue

            logging.debug(f"watch_pvcs: Pvc {pvc.metadata.name} {event['type']}")

            with self.lock:
                if event["type"] == "ADDED":
                    self.get_record(pvc.metadata.name)
                elif event["type"] == "DELETED":
                    self.delete_record(pvc.metadata.name)

    def delete_pvc(self, api_instance, namespace, pvc_name, dry_run):
        if dry_run:
            logging.info(f"Dry run enabled, would attempt to delete PVC: {pvc_name}")
        else:
            try:
                logging.info(f"Deleting PVC: {pvc_name}")
                api_instance.delete_namespaced_persistent_volume_claim(
                    pvc_name, namespace
                )
                logging.info(f"PVC {pvc_name} deleted successfully")
            except Exception as e:
                logging.error(f"Error deleting PVC {pvc_name}: {e}")


class KubernetesManager:
    def __init__(self, args):
        self.namespace = args.namespace
        self.threshold = args.timeout * 60
        self.pvc_filter = args.pvc_filter
        self.pod_filter = args.pod_filter
        self.polling_interval = args.polling_interval
        self.dry_run = args.dry_run

        client_configuration = client.Configuration()
        # This results in a total connection timeout of about 30 seconds
        client_configuration.retries = urllib3.util.Retry(total=5, backoff_factor=1)

        try:
            config.load_incluster_config(client_configuration=client_configuration)
        except Exception:
            config.load_kube_config(client_configuration=client_configuration)

        self.api_instance = client.CoreV1Api(client.ApiClient(client_configuration))
        self.pvc_db = PVCDatabase(
            self.api_instance,
            self.namespace,
            self.threshold,
            self.pvc_filter,
            self.pod_filter,
            self.dry_run,
        )


def main():
    parser = argparse.ArgumentParser(
        description="Delete PVCs that haven't been used for a certain amount of time"
    )
    parser.add_argument("-n", "--namespace", required=True, help="Kubernetes Namespace")
    parser.add_argument(
        "-t",
        "--timeout",
        type=int,
        default=240,
        help="Threshold in minutes for idle PVCs",
    )
    parser.add_argument(
        "--pod-filter",
        default="^.*$",
        help="Regular expression filtering pods within the namespace",
    )
    parser.add_argument(
        "--pvc-filter",
        default="^.*$",
        help="Regular expression filtering PVCs within the namespace",
    )

    parser.add_argument(
        "--polling-interval", type=int, default=30, help="Polling interval in seconds"
    )
    parser.add_argument(
        "--dry-run",
        action="store_true",
        help="Run in dry-run mode without actual deletion",
    )
    parser.add_argument(
        "--debug",
        action="store_true",
        help="Enable debug logging",
    )
    args = parser.parse_args()

    # Configure logging
    logging.basicConfig(
        level=logging.DEBUG if args.debug else logging.INFO,
        format="%(asctime)s.%(msecs)d [%(name)s] [%(levelname)s] - %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )
    if args.debug:
        # kubernetes.client.rest logs some noisy stuff at debug level,
        # so bump it back to INFO level
        logging.getLogger("kubernetes.client.rest").setLevel(logging.INFO)

    # Logging the arguments
    logging.info(f"namespace: {args.namespace}")
    logging.info(f"timeout: {args.timeout} minutes")
    logging.info(f"pod_filter: {args.pod_filter}")
    logging.info(f"pvc_filter: {args.pvc_filter}")
    logging.info(f"polling_interval: {args.polling_interval} seconds")
    logging.info(f"dry_run: {args.dry_run}")
    logging.info(f"debug: {args.debug}")

    k8s_manager = KubernetesManager(args)

    try:
        while (
            k8s_manager.pvc_db.worker_thread_exited.wait(args.polling_interval)
            is not True
        ):
            logging.debug("Checking for idle PVCs")
            if args.debug:
                logging.debug(k8s_manager.pvc_db)
            idle_pvcs = k8s_manager.pvc_db.get_idlers(k8s_manager.threshold)
            for pvc_name in idle_pvcs:
                logging.info(f"PVC {pvc_name} is eligible for deletion")
                k8s_manager.pvc_db.delete_pvc(
                    k8s_manager.api_instance,
                    k8s_manager.namespace,
                    pvc_name,
                    k8s_manager.dry_run,
                )
        # Reach here if a worker thread terminated.
        # Allow some extra time for the failing thread to fully unwind.
        time.sleep(1)
    except KeyboardInterrupt:
        logging.error("Interrupted")
    logging.error("Exiting")


if __name__ == "__main__":
    main()
