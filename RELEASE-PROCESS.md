# The k8s-pvc-cleaner release process

Welcome!

## Publish a container image

To build and publish a container image of k8s-pvc-cleaner, create a **protected tag**[^1] in this repository.  This is easy to do in [the Gitlab UI](https://gitlab.wikimedia.org/repos/releng/k8s-pvc-cleaner/-/tags/new).

Use the [pattern of the existing tags](https://gitlab.wikimedia.org/repos/releng/k8s-pvc-cleaner/-/tags) to choose a new tag name.

When the tag is created a pipeline will fire off which will eventually create and publish the image.  The image name will be:

`docker-registry.wikimedia.org/repos/releng/k8s-pvc-cleaner:TAG`

where `TAG` is the tag name you used.

## Deployment

k8s-pvc-cleaner is part of a collection of services deployed via [gitlab-cloud-runner](https://gitlab.wikimedia.org/repos/releng/gitlab-cloud-runner).  To make gitlab-cloud-runner use the new version of the k8s-pvc-cleaner image, set `image.tag: <new tag>` in <https://gitlab.wikimedia.org/repos/releng/gitlab-cloud-runner/-/blob/main/k8s-pvc-cleaner/k8s-pvc-cleaner-values.yaml.tftpl?ref_type=heads>, commit, and prepare a merge request in GitLab.  When the merge request is merged, [a pipeline](https://gitlab.wikimedia.org/repos/releng/gitlab-cloud-runner/-/pipelines) will fire off which will eventually pause at the `deploy-staging` job.  Manually trigger the `deploy-staging` job to deploy the changes to the staging cluster for testing.  Once testing in staging is satisfied, trigger the `push-to-production` job to complete the deployment.

## Update the helm chart

When you are satisfied with the quality of the new image, make it the recommended version in the helm chart.  This is the default version that will be deployed if no image tag is specified in values.yaml

In helm/Chart.yaml (in this k8s-pvc-cleaner repo), update `appVersion` with the new recommended tag.  This means you also need to advance the `version` field.  Commit, and prepare a merge request in GitLab. When the merge request is merged, a pipeline will fire off which will publish the updated helm chart.

[^1]: This repository _should_ be configured to protect all tags using the `*` wildcard.  You can verify in <https://gitlab.wikimedia.org/repos/releng/k8s-pvc-cleaner/-/settings/repository>
