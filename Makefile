IMAGE := pvc-cleaner:latest
SMOKETEST_IMAGE := pvc-cleaner:smoketest
LINT_IMAGE := pvc-cleaner:lint

.PHONY: smoketest smoketest-image image reformat lint-image lint

smoketest: smoketest-image
	docker run --rm $(SMOKETEST_IMAGE)

smoketest-image:
	docker buildx build -f .pipeline/blubber.yaml --target smoketest --tag $(SMOKETEST_IMAGE) .

image:
	docker buildx build -f .pipeline/blubber.yaml --target image --tag $(IMAGE) .

lint-image:
	docker buildx build -f .pipeline/blubber.yaml --target lint --tag $(LINT_IMAGE) .

lint: lint-image
	docker run --rm $(LINT_IMAGE)

reformat: 
	docker run -it --rm --entrypoint /srv/app/.tox/lint/bin/black --user $$(id -u):$$(id -g) -v $$(pwd):/source -w /source $(LINT_IMAGE) pvc-cleaner.py
